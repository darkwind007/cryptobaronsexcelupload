﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Excel_Upload.Models
{
   public class Buildings
    {
        public float Id { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public bool Upgradable { get; set; }
        public float BuildingTimeHours { get; set; }
        public float? UpgradeTimeHours { get; set; }
        public int BuildResourceWood { get; set; }
        public int BuildResourceFood { get; set; }
        public int BuildResourceGold { get; set; }
        public int? UpgradeWoodCost { get; set; }
        public int? UpgradeFoodCost { get; set; }
        public int? UpgradeGoldCost { get; set; }
        public int TownHallLevel { get; set; }
        public int MaximumWoodStored { get; set; }
        public int MaximumFoodStored { get; set; }
        public int MaximumGoldStored { get; set; }
        public int HousingCapacity { get; set; }
        public int ProdWoodPerHour { get; set; }
        public int ProdFoodPerHour { get; set; }
        public int ProdGoldPerHour { get; set; }
        public int HitPoint { get; set; }
        public int Damage { get; set; }
        public bool IsDefence { get; set; }
        public bool IsProduction { get; set; }
        public bool IsStorage { get; set; }
        public bool GiveReward { get; set; }
        public float FireRate { get; set; }

    }
}