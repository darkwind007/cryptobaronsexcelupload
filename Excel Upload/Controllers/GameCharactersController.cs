﻿using Excel_Upload.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Excel_Upload.Controllers
{
    public class GameCharactersController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Characters()
        {
            string pathForSaving = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ExcelUploads");
            string savedFileName = Path.Combine(pathForSaving, "CryptobaronCharacters.xls");
            DataSet studentSet = Utility.ReadExcel(savedFileName);
            int rowCount = studentSet.Tables[0].Rows.Count;

            List<Characters> characters = new List<Characters>();

            for (int i = 0; i < rowCount; i++)
            {

                if (studentSet.Tables[0].Rows[i][0] != null)
                {

                    Characters character = new Characters();
                    string Id = studentSet.Tables[0].Rows[i][0].ToString().Trim();
                    if (!string.IsNullOrEmpty(Id))
                    {
                        character.Id = Convert.ToInt32(Id);
                    }

                    string Name = studentSet.Tables[0].Rows[i][1].ToString().Trim();
                    if (!string.IsNullOrEmpty(Name))
                    {
                        character.Name = Name;
                    }

                    string Level = studentSet.Tables[0].Rows[i][2].ToString().Trim();
                    if (!string.IsNullOrEmpty(Level))
                    {
                        character.Level = Convert.ToInt32(Level);
                    }

                    string Upgradable = studentSet.Tables[0].Rows[i][3].ToString().Trim();
                    if (!string.IsNullOrEmpty(Upgradable))
                    {
                        character.Upgradable = Convert.ToBoolean(Upgradable);
                    }

                    string HitPoint = studentSet.Tables[0].Rows[i][4].ToString().Trim();
                    if (!string.IsNullOrEmpty(HitPoint))
                    {
                        character.HitPoint = Convert.ToInt32(HitPoint);
                    }
                    else
                    {
                        character.HitPoint = 0;
                    }
                    string Damage = studentSet.Tables[0].Rows[i][5].ToString().Trim();
                    if (!string.IsNullOrEmpty(Damage))
                    {
                        character.Damage = Convert.ToInt32(Damage);
                    }
                    else
                    {
                        character.Damage = 0;
                    }
                    string TrainingCostFood = studentSet.Tables[0].Rows[i][6].ToString().Trim();
                    if (!string.IsNullOrEmpty(TrainingCostFood))
                    {
                        character.TrainingCostFood = Convert.ToInt32(TrainingCostFood);
                    }
                    else
                    {
                        character.TrainingCostFood = 0;
                    }

                    string TrainingCostGold = studentSet.Tables[0].Rows[i][7].ToString().Trim();
                    if (!string.IsNullOrEmpty(TrainingCostGold))
                    {
                        character.TrainingCostGold = Convert.ToInt32(TrainingCostGold);
                    }
                    else
                    {
                        character.TrainingCostGold = 0;
                    }

                    string UpgradableTimeMinutes = studentSet.Tables[0].Rows[i][8].ToString().Trim();
                    if (!string.IsNullOrEmpty(UpgradableTimeMinutes))
                    {
                        character.UpgradableTimeMinutes = float.Parse(UpgradableTimeMinutes);
                    }
                    else
                    {
                        character.UpgradableTimeMinutes = 0;
                    }

                    string UpgradableCostFood = studentSet.Tables[0].Rows[i][9].ToString().Trim();
                    if (!string.IsNullOrEmpty(UpgradableCostFood))
                    {
                        character.UpgradableCostFood = Convert.ToInt32(UpgradableCostFood);
                    }
                    else
                    {
                        character.UpgradableCostFood = 0;
                    }
                    string UpgradableCostGold = studentSet.Tables[0].Rows[i][10].ToString().Trim();
                    if (!string.IsNullOrEmpty(UpgradableCostGold))
                    {
                        character.UpgradableCostGold = Convert.ToInt32(UpgradableCostGold);
                    }
                    else
                    {
                        character.UpgradableCostGold = 0;
                    }
                    string HealingStrength = studentSet.Tables[0].Rows[i][11].ToString().Trim();
                    if (!string.IsNullOrEmpty(HealingStrength))
                    {
                        character.HealingStrength = Convert.ToInt32(HealingStrength);
                    }
                    else
                    {
                        character.HealingStrength = 0;
                    }

                    characters.Add(character);
                }
            }
            return Ok(characters);
        }
    }
}
