﻿using Excel_Upload.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace Excel_Upload.Controllers
{
    public class HomeController : Controller
    {
        private string DarkWalletMainProduction = "http://216.117.171.173/";
        private string DarkWalletTestProduction = "http://216.117.144.224/";
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        // GET: /Home/
        public async System.Threading.Tasks.Task<ActionResult> IndexAsync()
        {
            //var model = new MyViewModel();
            string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
            string savedFileName = Path.Combine(pathForSaving, "cryptobaronbounty.xls");
            //hpf.SaveAs(savedFileName);
            DataSet studentSet = ReadExcel(savedFileName);
            int rowCount = studentSet.Tables[0].Rows.Count;

            List<AirdropInformation> airDropInformation = new List<AirdropInformation>();

            for (int i = 0; i < rowCount; i++)
            {

                if (studentSet.Tables[0].Rows[i][0] != null)
                {
                    airDropInformation.Add(new AirdropInformation()
                    {
                        EthereumAddress = studentSet.Tables[0].Rows[i][0].ToString().Trim(),
                        Amount = Convert.ToDecimal(studentSet.Tables[0].Rows[i][2])
                    });
                }



            }
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://216.117.171.173/");

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            /*client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessTokenResponse?.access_token);*/

            HttpContent _Body = new StringContent(JsonConvert.SerializeObject(airDropInformation));

            _Body.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/Airdrop/ProcessAirdrop", airDropInformation);
            var response = responseMessage.Content.ReadAsAsync<Dictionary<string, string>>();
            var data =  response.Result;
            foreach (var item in data)
            {
                Console.WriteLine($"{item.Key}  {item.Value}");
            }
            return View("Index",data);
        }
        private DataSet ReadExcel(string filepath)
        {
            DataSet Result = null;
            try
            {
                string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + filepath + ";" + "Extended Properties=Excel 8.0;";
                OleDbConnection connection = new OleDbConnection(xConnStr);

                connection.Open();
                DataTable sheet = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                foreach (DataRow dataRow in sheet.Rows)
                {
                    string sheetName = dataRow[2].ToString().Replace("'", "");
                    OleDbCommand command = new OleDbCommand("Select * FROM [" + sheetName + "]", connection);
                    // Create DbDataReader to Data Worksheet

                    OleDbDataAdapter MyData = new OleDbDataAdapter();
                    MyData.SelectCommand = command;
                    DataSet ds = new DataSet();
                    ds.Clear();
                    MyData.Fill(ds);
                    connection.Close();

                    Result = ds;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;

        }
    }
}