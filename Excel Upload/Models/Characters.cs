﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Excel_Upload.Models
{
    public class Characters
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public bool Upgradable { get; set; }
        public int? HitPoint { get; set; }
        public int? Damage { get; set; }
        public int? TrainingCostFood { get; set; }
        public int? TrainingCostGold { get; set; }
        public float? UpgradableTimeMinutes { get; set; }
        public int? UpgradableCostFood { get; set; }
        public int? UpgradableCostGold { get; set; }
        public int? HealingStrength { get; set; }
    }
}