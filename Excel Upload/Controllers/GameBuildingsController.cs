﻿using Excel_Upload.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Excel_Upload.Controllers
{
    public class GameBuildingsController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Buildings()
        {
            string pathForSaving = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ExcelUploads");
            string savedFileName = Path.Combine(pathForSaving, "CryptobaronBuildings.xls");
            DataSet studentSet = Utility.ReadExcel(savedFileName);
            int rowCount = studentSet.Tables[0].Rows.Count;

            List<Buildings> buildings = new List<Buildings>();

            for (int i = 0; i < rowCount; i++)
            {

                if (studentSet.Tables[0].Rows[i][0] != null)
                {

                    Buildings building = new Buildings();
                    string Id = studentSet.Tables[0].Rows[i][0].ToString().Trim();
                    if (!string.IsNullOrEmpty(Id))
                    {
                        building.Id = Convert.ToInt32(Id);
                    }
                    else
                    {
                        building.Id = 0;
                    }
                    string Name = studentSet.Tables[0].Rows[i][1].ToString().Trim();
                    if (!string.IsNullOrEmpty(Name))
                    {
                        building.Name = Name;
                    }
                    else
                    {
                        building.Name = "0";
                    }

                    string Level = studentSet.Tables[0].Rows[i][2].ToString().Trim();
                    if (!string.IsNullOrEmpty(Level))
                    {
                        building.Level = Convert.ToInt32(Level);
                    }
                    else
                    {
                        building.Level = 0;
                    }

                    string Upgradable = studentSet.Tables[0].Rows[i][3].ToString().Trim();
                    if (!string.IsNullOrEmpty(Upgradable))
                    {
                        building.Upgradable = Convert.ToBoolean(Upgradable);
                    }
                    else
                    {
                        building.Upgradable = false;
                    }

                    string BuildingTimeHours = studentSet.Tables[0].Rows[i][4].ToString().Trim();
                    if (!string.IsNullOrEmpty(BuildingTimeHours))
                    {
                        building.BuildingTimeHours = float.Parse(BuildingTimeHours);
                    }
                    else
                    {
                        building.BuildingTimeHours = 0;
                    }

                    string UpgradeTimeHours = studentSet.Tables[0].Rows[i][5].ToString().Trim();
                    if (string.IsNullOrEmpty(UpgradeTimeHours))
                    {
                        building.UpgradeTimeHours = 0;
                    }
                    else
                    {
                        building.UpgradeTimeHours = float.Parse(UpgradeTimeHours);
                    }

                    string BuildResourceWood = studentSet.Tables[0].Rows[i][6].ToString().Trim();
                    if (string.IsNullOrEmpty(BuildResourceWood))
                    {
                        building.BuildResourceWood = 0;
                    }
                    else
                    {
                        building.BuildResourceWood = Convert.ToInt32(BuildResourceWood);
                    }

                    string BuildResourceFood = studentSet.Tables[0].Rows[i][7].ToString().Trim();
                    if (string.IsNullOrEmpty(BuildResourceFood))
                    {
                        building.BuildResourceFood = 0;
                    }
                    else
                    {
                        building.BuildResourceFood = Convert.ToInt32(BuildResourceFood);
                    }

                    string BuildResourceGold = studentSet.Tables[0].Rows[i][8].ToString().Trim();
                    if (string.IsNullOrEmpty(BuildResourceGold))
                    {
                        building.BuildResourceGold = 0;
                    }
                    else
                    {
                        building.BuildResourceGold = Convert.ToInt32(BuildResourceGold);
                    }

                    string UpgradeWoodCost = studentSet.Tables[0].Rows[i][9].ToString().Trim();
                    if (string.IsNullOrEmpty(UpgradeWoodCost))
                    {
                        building.UpgradeWoodCost = 0;
                    }
                    else
                    {
                        building.UpgradeWoodCost = Convert.ToInt32(UpgradeWoodCost);
                    }
                    string UpgradeFoodCost = studentSet.Tables[0].Rows[i][10].ToString().Trim();
                    if (string.IsNullOrEmpty(UpgradeFoodCost))
                    {
                        building.UpgradeFoodCost = 0;
                    }
                    else
                    {
                        building.UpgradeFoodCost = Convert.ToInt32(UpgradeFoodCost);
                    }
                    string UpgradeGoldCost = studentSet.Tables[0].Rows[i][11].ToString().Trim();
                    if (string.IsNullOrEmpty(UpgradeGoldCost))
                    {
                        building.UpgradeGoldCost = 0;
                    }
                    else
                    {
                        building.UpgradeGoldCost = Convert.ToInt32(UpgradeGoldCost);
                    }


                    string TownHallLevel = studentSet.Tables[0].Rows[i][12].ToString().Trim();
                    if (!string.IsNullOrEmpty(TownHallLevel))
                    {
                        building.TownHallLevel = Convert.ToInt32(TownHallLevel);
                    }

                    string MaximumWoodStored = studentSet.Tables[0].Rows[i][13].ToString().Trim();
                    if (!string.IsNullOrEmpty(MaximumWoodStored))
                    {
                        building.MaximumWoodStored = Convert.ToInt32(MaximumWoodStored);
                    }

                    string MaximumFoodStored = studentSet.Tables[0].Rows[i][14].ToString().Trim();
                    if (!string.IsNullOrEmpty(MaximumFoodStored))
                    {
                        building.MaximumFoodStored = Convert.ToInt32(MaximumFoodStored);
                    }
                    string MaximumGoldStored = studentSet.Tables[0].Rows[i][15].ToString().Trim();
                    if (!string.IsNullOrEmpty(MaximumGoldStored))
                    {
                        building.MaximumGoldStored = Convert.ToInt32(MaximumGoldStored);
                    }

                    string HousingCapacity = studentSet.Tables[0].Rows[i][16].ToString().Trim();
                    if (!string.IsNullOrEmpty(HousingCapacity))
                    {
                        building.HousingCapacity = Convert.ToInt32(HousingCapacity);
                    }

                    string ProdWoodPerHour = studentSet.Tables[0].Rows[i][17].ToString().Trim();
                    if (!string.IsNullOrEmpty(ProdWoodPerHour))
                    {
                        building.ProdWoodPerHour = Convert.ToInt32(ProdWoodPerHour);
                    }

                    string ProdFoodPerHour = studentSet.Tables[0].Rows[i][18].ToString().Trim();
                    if (!string.IsNullOrEmpty(ProdFoodPerHour))
                    {
                        building.ProdFoodPerHour = Convert.ToInt32(ProdFoodPerHour);
                    }

                    string ProdGoldPerHour = studentSet.Tables[0].Rows[i][19].ToString().Trim();
                    if (!string.IsNullOrEmpty(ProdGoldPerHour))
                    {
                        building.ProdGoldPerHour = Convert.ToInt32(ProdGoldPerHour);
                    }

                    string HitPoint = studentSet.Tables[0].Rows[i][20].ToString().Trim();
                    if (!string.IsNullOrEmpty(HitPoint))
                    {
                        building.HitPoint = Convert.ToInt32(HitPoint);
                    }

                    string Damage = studentSet.Tables[0].Rows[i][21].ToString().Trim();
                    if (!string.IsNullOrEmpty(Damage))
                    {
                        building.Damage = Convert.ToInt32(Damage);
                    }

                    string IsDefence = studentSet.Tables[0].Rows[i][22].ToString().Trim();
                    if (!string.IsNullOrEmpty(IsDefence))
                    {
                        building.IsDefence = Convert.ToBoolean(IsDefence);
                    }
                    string IsProduction = studentSet.Tables[0].Rows[i][23].ToString().Trim();
                    if (!string.IsNullOrEmpty(IsProduction))
                    {
                        building.IsProduction = Convert.ToBoolean(IsProduction);
                    }
                    string IsStorage = studentSet.Tables[0].Rows[i][24].ToString().Trim();
                    if (!string.IsNullOrEmpty(IsStorage))
                    {
                        building.IsStorage = Convert.ToBoolean(IsStorage);
                    }
                    string GiveReward = studentSet.Tables[0].Rows[i][25].ToString().Trim();
                    if (!string.IsNullOrEmpty(GiveReward))
                    {
                        building.GiveReward = Convert.ToBoolean(GiveReward);
                    }

                    string FireRate = studentSet.Tables[0].Rows[i][26].ToString().Trim();
                    if (!string.IsNullOrEmpty(FireRate))
                    {
                        building.FireRate = float.Parse(FireRate);
                    }
                    else
                    {
                        building.FireRate = 0;
                    }
                    buildings.Add(building);

                }
            }

            return Ok(buildings);
        }
    }
}
