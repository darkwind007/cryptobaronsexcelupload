﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Excel_Upload.Models
{
    public class AirdropInformation
    {
        public string EthereumAddress { get; set; }
        public decimal Amount { get; set; }
    }
}